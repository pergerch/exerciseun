﻿namespace Tasker.Data
{
	using Microsoft.EntityFrameworkCore;
	using Tasker.Data.Entities;

	public class TaskerContext : DbContext
	{
		public TaskerContext(DbContextOptions<TaskerContext> options)
			: base(options)
		{
			// TODO: Not for production!
			////Database.EnsureDeleted();

			Database.Migrate();
		}

		public DbSet<Staff> Staffs { get; set; }

		public DbSet<Task> Tasks { get; set; }

		protected override void OnModelCreating(ModelBuilder modelBuilder)
		{
			modelBuilder.Entity<Staff>().HasData(new Staff()
			{
				Name = "Adam",
				StaffId = -1,
			}, new Staff()
			{
				Name = "Bob",
				StaffId = -2,
			}, new Staff()
			{
				Name = "Chris",
				StaffId = -3,
			}, new Staff()
			{
				Name = "Dave",
				StaffId = -4,
			}, new Staff()
			{
				Name = "Erik",
				StaffId = -5,
			});
		}
	}
}