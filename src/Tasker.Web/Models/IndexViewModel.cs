﻿namespace Tasker.Web.Models
{
	using System.Collections.Generic;

	public class IndexViewModel
	{
		public IndexViewModel()
		{
			Staff = new List<string>();
			Tasks = new List<TaskViewModel>();
		}

		public IEnumerable<string> Staff { get; set; }

		public IEnumerable<TaskViewModel> Tasks { get; set; }
	}
}