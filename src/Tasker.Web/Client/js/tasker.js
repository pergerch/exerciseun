﻿(function (scope) {
   "use strict";

   scope.tasker = (function () {

	  var self = {};

	  //self.tasksContainer = "#taskerPage";


	  self.initialize = function (data, baseUrl) {
		 var viewModel = {};

		 viewModel.tasks = ko.observableArray(convertToObservable(data.tasks));
		 viewModel.staff = data.staff;
		 viewModel.removeTask = function (task) {
			if (confirm("Are you sure?")) {
			   $.ajax({
				  url: baseUrl + task.taskId(),
				  type: "DELETE",
				  success: function (result) {
					 viewModel.tasks.remove(task);

					 toastr.success("Task deleted!");
				  },
				  error: function () {
					 toastr.error("Delete failed.");
				  }
			   });
			   viewModel.tasks.remove(task);
			}
		 };

		 viewModel.startTask = function (task) {
			$.post(baseUrl + task.taskId() + "/start", null, function (data) {
				  task.status(2);
			   });
		 };
		 viewModel.stopTask = function (task) {
			$.post(baseUrl + task.taskId() + "/stop", null, function (data) {
				  task.status(1);
			   });
		 };
		 viewModel.doneTask = function (task) {
			$.post(baseUrl + task.taskId() + "/done", null, function (data) {
				  task.status(3);
			   });
		 };
		 viewModel.lowPriority = function (task) {
			$.post(baseUrl + task.taskId() + "/Priority/1", null, function (data) {
				  task.priority(1);
			   });
		 };
		 viewModel.mediumPriority = function (task) {
			$.post(baseUrl + task.taskId() + "/Priority/2", null, function (data) {
				  task.priority(2);
			   });
		 };
		 viewModel.highPriority = function (task) {
			$.post(baseUrl + task.taskId() + "/Priority/3", null, function (data) {
				  task.priority(3);
			   });
		 };

		 ko.applyBindings(viewModel);

		 $("#createTaskForm").validate({
			submitHandler: function () {
				$.post("/api/tasks", {
				   title: $("#title").val(),
				   dueDate: $("#dueDate").val(),
				   priority: $("input[name='priority']:checked").val(),
				   staff: $("#staff").val()
				}, function (data) {
				   var observableData = convertToObservable([data]);
				   viewModel.tasks.push(observableData[0]);

				   toastr.success("Task created!");
				}).fail(function (data) {
				   toastr.error(data.responseText, "Couldn't create task.");
				});
			}
		});
		};

	  self.GetStatus = function (status) {
		 switch (status()) {
			case 1:
			   return "Not Started";
			case 2:
			   return "In Progress";
			case 3:
			   return "Done";
		 }
	  };

	  self.GetPriority = function (priority) {
		 switch (priority()) {
			case 1:
			   return "Low";
			case 2:
			   return "Medium";
			case 3:
			   return "High";
		 }
	  }

	  function convertToObservable(list) {
		 var newList = [];
		 $.each(list, function (i, obj) {
			var newObj = {};
			Object.keys(obj).forEach(function (key) {
			   newObj[key] = ko.observable(obj[key]);
			});
			newList.push(newObj);
		 });
		 return newList;
	  }

	  return self;

   })();

})(window);