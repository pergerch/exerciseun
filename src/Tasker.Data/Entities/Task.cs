﻿namespace Tasker.Data.Entities
{
	using System;

	public class Task
	{
		public DateTime DueDate { get; set; }

		public PriorityEnum Priority { get; set; }

		public Staff Staff { get; set; }

		public StatusEnum Status { get; set; }

		public int TaskId { get; set; }

		public string Title { get; set; }
	}
}