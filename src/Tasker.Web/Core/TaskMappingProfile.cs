﻿namespace Tasker.Web.Core
{
	using AutoMapper;
	using Tasker.Data.Entities;
	using Tasker.Web.Models;

	public class TaskMappingProfile : Profile
	{
		public TaskMappingProfile()
		{
			CreateMap<Task, TaskViewModel>().ForMember(dest => dest.Staff, opt => opt.MapFrom(src => src.Staff.Name));

			CreateMap<StatusEnum, StatusEnumViewModel>().ReverseMap();
			CreateMap<PriorityEnum, PriorityEnumViewModel>().ReverseMap();

			CreateMap<Staff, string>().ConvertUsing(x => x.Name);
		}
	}
}