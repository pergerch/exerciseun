﻿namespace Tasker.Web.Models
{
	using System;
	using System.ComponentModel.DataAnnotations;

	public class TaskCreateViewModel
	{
		[Required]
		public DateTime DueDate { get; set; }

		[Required]
		public string Title { get; set; }

		[Required]
		public string Priority { get; set; }

		[Required]
		public string Staff { get; set; }
	}
}