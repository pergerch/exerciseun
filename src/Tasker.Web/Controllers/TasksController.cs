﻿namespace Tasker.Web.Controllers
{
	using System;
	using System.Linq;
	using System.Threading.Tasks;
	using AutoMapper;
	using Microsoft.AspNetCore.Mvc;
	using Microsoft.EntityFrameworkCore;
	using Tasker.Data;
	using Tasker.Data.Entities;
	using Tasker.Web.Models;

	[Route("api/[controller]")]
	[ApiController]
	public class TasksController : ControllerBase
	{
		public TasksController(TaskerContext context, IMapper mapper)
		{
			Context = context;
			Mapper = mapper;
		}

		private TaskerContext Context { get; }

		private IMapper Mapper { get; }

		[HttpPost("")]
		public async Task<IActionResult> AddTask([FromForm] TaskCreateViewModel taskViewModel)
		{
			PriorityEnumViewModel priority;
			if (!Enum.TryParse(taskViewModel.Priority, true, out priority))
			{
				return BadRequest("Priority invalid.");
			}

			Staff staff = Context.Staffs.SingleOrDefault(x => x.Name == taskViewModel.Staff);
			if (staff == null)
			{
				return BadRequest("Staff invalid.");
			}

			Data.Entities.Task task = new Data.Entities.Task()
			{
				DueDate = taskViewModel.DueDate,
				Title = taskViewModel.Title,
				Priority = Mapper.Map<PriorityEnum>(priority),
				Staff = staff,
				Status = StatusEnum.NotStarted,
			};

			await Context.Tasks.AddAsync(task);
			await Context.SaveChangesAsync();

			return Created(Url.Action("GetTask", "Tasks", new
			{
				id = task.TaskId,
			}), Mapper.Map<TaskViewModel>(task));
		}

		[HttpDelete("{id}")]
		public async Task<IActionResult> DeleteTask(int id)
		{
			Data.Entities.Task task = Context.Tasks.SingleOrDefault(x => x.TaskId == id);
			if (task == null)
			{
				return NotFound();
			}

			Context.Tasks.Remove(task);
			await Context.SaveChangesAsync();

			return Ok();
		}

		[HttpPost("{id}/done")]
		public async Task<IActionResult> DoneTask(int id)
		{
			Data.Entities.Task task = Context.Tasks.SingleOrDefault(x => x.TaskId == id);
			if (task == null)
			{
				return NotFound();
			}

			task.Status = StatusEnum.Done;
			await Context.SaveChangesAsync();

			return Ok();
		}

		[HttpGet("{id}")]
		public async Task<IActionResult> GetTask(int id)
		{
			Data.Entities.Task task = await Context.Tasks.SingleOrDefaultAsync(x => x.TaskId == id);
			if (task == null)
			{
				return NotFound();
			}

			return Ok(Mapper.Map<TaskViewModel>(task));
		}

		[HttpPost("{id}/priority/{priority}")]
		public async Task<IActionResult> SetPriority(int id, int priority)
		{
			Data.Entities.Task task = Context.Tasks.SingleOrDefault(x => x.TaskId == id);
			if (task == null)
			{
				return NotFound();
			}

			task.Priority = (PriorityEnum)priority;
			await Context.SaveChangesAsync();

			return Ok();
		}

		[HttpPost("{id}/start")]
		public async Task<IActionResult> StartTask(int id)
		{
			Data.Entities.Task task = Context.Tasks.Include(x => x.Staff).SingleOrDefault(x => x.TaskId == id);
			if (task == null)
			{
				return NotFound();
			}

			// Staff member has already a task in progress?
			if (Context.Tasks.Include(x => x.Staff).Any(x => x.Status == StatusEnum.InProgress && x.Staff.StaffId == task.Staff.StaffId))
			{
				return BadRequest("Staff already busy.");
			}

			task.Status = StatusEnum.InProgress;
			await Context.SaveChangesAsync();

			return Ok();
		}

		[HttpPost("{id}/stop")]
		public async Task<IActionResult> StopTask(int id)
		{
			Data.Entities.Task task = Context.Tasks.SingleOrDefault(x => x.TaskId == id);
			if (task == null)
			{
				return NotFound();
			}

			task.Status = StatusEnum.NotStarted;
			await Context.SaveChangesAsync();

			return Ok();
		}
	}
}