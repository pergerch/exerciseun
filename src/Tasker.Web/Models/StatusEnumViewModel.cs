﻿namespace Tasker.Web.Models
{
	public enum StatusEnumViewModel
	{
		NotStarted = 1,

		InProgress,

		Done,
	}
}