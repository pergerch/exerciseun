﻿namespace Tasker.Data.Migrations
{
	using System;
	using Microsoft.EntityFrameworkCore.Metadata;
	using Microsoft.EntityFrameworkCore.Migrations;

	public partial class InitialModel : Migration
	{
		protected override void Down(MigrationBuilder migrationBuilder)
		{
			migrationBuilder.DropTable("Tasks");

			migrationBuilder.DropTable("Staffs");
		}

		protected override void Up(MigrationBuilder migrationBuilder)
		{
			migrationBuilder.CreateTable("Staffs", table => new
			{
				Name = table.Column<string>(nullable: true),
				StaffId = table.Column<int>(nullable: false)
					.Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
			}, constraints: table => { table.PrimaryKey("PK_Staffs", x => x.StaffId); });

			migrationBuilder.CreateTable("Tasks", table => new
			{
				DueDate = table.Column<DateTime>(nullable: false),
				Priority = table.Column<int>(nullable: false),
				StaffId = table.Column<int>(nullable: true),
				Status = table.Column<int>(nullable: false),
				TaskId = table.Column<int>(nullable: false)
					.Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
				Title = table.Column<string>(nullable: true),
			}, constraints: table =>
			{
				table.PrimaryKey("PK_Tasks", x => x.TaskId);
				table.ForeignKey("FK_Tasks_Staffs_StaffId", x => x.StaffId, "Staffs", "StaffId", onDelete: ReferentialAction.Restrict);
			});

			migrationBuilder.InsertData("Staffs", new[] { "StaffId", "Name" },
				new object[,] { { -1, "Adam" }, { -2, "Bob" }, { -3, "Chris" }, { -4, "Dave" }, { -5, "Erik" } });

			migrationBuilder.CreateIndex("IX_Tasks_StaffId", "Tasks", "StaffId");
		}
	}
}