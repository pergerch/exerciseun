# Readme

## Prerequisites

### .NET Core

Make sure you have installed the .NET Core 2.1 SDK, found [here](https://www.microsoft.com/net/download/dotnet-core/2.1).

### npm

The solution is using the node package manager. Please make sure you have it installed. The download can be found [here](https://nodejs.org/en/download/).

### Database

The solution expects a database to be accessible via `localhost\SQLExpress` with the database name `tasks` and integrated security. This can be changed in the `appsettings.json`.

If the provided database is empty, the structure will be created at startup automatically through EF migrations.

### Logging (optional)

The solution uses seq as logging solution. To see the logging output either [download](https://getseq.net/Download) and install seq locally (http://localhost:5341) or configure the server url in the `appsettings.json`.

## Running the project

### Installing javascript packages

Run `npm i` from the solution root folder in the console window to download all required javascript packages.

### Bundling

The solution uses **Webpack** as module bundler. The `package.json` defines 3 bundling actions:

- Bundling for production: `npm run-script prod`
- Bundling for development: `npm run-script build`
- Bundling while developing: `npm run-script start`

*Note:* The latter one is recommended when still developing. The bundler watches the source files and bundles as soon as one file changes.

### Run the solution

Run `dotnet build` and `dotnet run` from the solution root folder in the console window.

The output of the command should tell you where the website can be accessed. E.g. this is the default:

- Now listening on: https://localhost:5001
- Now listening on: http://localhost:5000

*Note:* For security and data protection reasons, HTTPS is enforced. HTTP automatically redirects to HTTPS. Use of the HTTPS version requires you to trust the development certificate. Do so by running `dotnet dev-certs https --trust`.

*Note:* `dotnet run` defaults to the development environment. For production (to hide detailed exception information), please use `dotnet run --environment=Production`.