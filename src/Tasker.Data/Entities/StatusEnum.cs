﻿namespace Tasker.Data.Entities
{
	public enum StatusEnum
	{
		NotStarted = 1,

		InProgress,

		Done,
	}
}