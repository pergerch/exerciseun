﻿namespace Tasker.Web.Controllers
{
	using System.Collections.Generic;
	using System.Diagnostics;
	using System.Linq;
	using AutoMapper;
	using Microsoft.AspNetCore.Mvc;
	using Microsoft.EntityFrameworkCore;
	using Tasker.Data;
	using Tasker.Web.Models;

	public class HomeController : Controller
	{
		public HomeController(TaskerContext context, IMapper mapper)
		{
			Context = context;
			Mapper = mapper;
		}

		private TaskerContext Context { get; }

		private IMapper Mapper { get; }

		[ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
		public IActionResult Error()
		{
			return View(new ErrorViewModel
			{
				RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier,
			});
		}

		public IActionResult Index()
		{
			List<TaskViewModel> tasks = Context.Tasks.Include(x => x.Staff).Select(x => Mapper.Map<TaskViewModel>(x)).ToList();
			List<string> staff = Context.Staffs.Select(x => Mapper.Map<string>(x)).ToList();

			return View(new IndexViewModel()
			{
				Staff = staff,
				Tasks = tasks,
			});
		}
	}
}