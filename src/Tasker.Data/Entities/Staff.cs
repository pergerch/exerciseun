﻿namespace Tasker.Data.Entities
{
	public class Staff
	{
		public string Name { get; set; }

		public int StaffId { get; set; }
	}
}