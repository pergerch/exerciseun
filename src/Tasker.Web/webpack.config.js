var path = require('path');
var webpack = require('webpack');
var UglifyJsPlugin = require('uglifyjs-webpack-plugin');

module.exports = function (env) {

	env = env || {};
	var isProd = env.NODE_ENV === 'production';

	// Setup base config for all environments
	var config = {
		entry: {
			main: './Client/js/main'
		},
		output: {
			path: path.resolve('wwwroot/dist'),
			filename: '[name].js'
		},
		devtool: 'eval-source-map',
		resolve: {
			extensions: ['.js']
		},
		plugins: [
		   new webpack.ProvidePlugin({ $: 'jquery', jQuery: 'jquery', ko: 'knockout', toastr: 'toastr' })
		],
		module: {
			rules: [
				{ test: /\.css?$/, use: ['style-loader', 'css-loader'] },
				{ test: /\.(png|jpg|jpeg|gif|svg)$/, use: 'url-loader?limit=25000' },
				{ test: /\.(png|woff|woff2|eot|ttf|svg)(\?|$)/, use: 'url-loader?limit=100000' },
				{
					test: require.resolve('jquery'),
					use: [
						{ loader: 'expose-loader', options: 'jQuery' },
						{ loader: 'expose-loader', options: '$' },
						{ loader: 'expose-loader', options: 'toastr' },
						{ loader: 'expose-loader', options: 'ko' }
					]
				}
			]
		}
	};

	// Alter config for prod environment
	if (isProd) {
		config.devtool = 'source-map';
		config.plugins = config.plugins.concat([
			new UglifyJsPlugin()
		]);
	}

	return config;
};