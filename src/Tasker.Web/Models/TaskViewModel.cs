﻿namespace Tasker.Web.Models
{
	using System;

	public class TaskViewModel
	{
		public DateTime DueDate { get; set; }

		public PriorityEnumViewModel Priority { get; set; }

		public string Staff { get; set; }

		public StatusEnumViewModel Status { get; set; }

		public int TaskId { get; set; }

		public string Title { get; set; }
	}
}